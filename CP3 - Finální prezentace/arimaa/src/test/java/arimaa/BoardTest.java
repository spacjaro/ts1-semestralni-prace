package arimaa;

import arimaa.enums.PieceColor;
import arimaa.enums.PieceType;
import arimaa.models.ArimaaGameRecorder;
import arimaa.models.Board;
import arimaa.models.Piece;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;

import java.util.List;

public class BoardTest {
    private Board board;
    private ArimaaGameRecorder mockArimaaGameRecorder;

    @BeforeEach
    public void setUp() {
        mockArimaaGameRecorder = mock(ArimaaGameRecorder.class);
        board = new Board(mockArimaaGameRecorder);
    }

    // ? Naming convention - NázevMetody_TestovanýStav_OčekávanýVýstup
    // Example -  isAdult_AgeLessThan18_False

    @Test
    public void getPieceAt_ValidPositionRow0Col0_Piece() {
        Piece piece = Mockito.mock(Piece.class);
        board.setPiece(piece, 0, 0);

        Piece result = board.getPieceAt(0, 0);

        assertEquals(piece, result);
    }

    @Test
    public void getPieceAt_PositionExceedsBoardSizeRow8Col8_ThrowsIndexOutOfBoundsException() {
        assertThrows(IndexOutOfBoundsException.class, () -> {
            // board.getPieceAt(board.getRowSize(), board.getColSize());
            board.getPieceAt(8, 8);
        });
    }

    @Test
    public void getPieceAt_PositionExceedsBoardSizeRowNegative1ColNegative1_ThrowsIndexOutOfBoundsException() {
        assertThrows(IndexOutOfBoundsException.class, () -> {
            board.getPieceAt(-1, -1);
        });
    }


    @Test
    public void setPiece_ValidPositionRow0Col0_Void() {
        Piece piece = Mockito.mock(Piece.class);

        board.setPiece(piece, 0, 0);

        Piece result = board.getPieceAt(0, 0);
        assertEquals(piece, result);
    }

    @Test
    public void setPiece_PositionExceedsBoardSizeRow8Col8_ThrowsIndexOutOfBoundsException() {
        Piece piece = Mockito.mock(Piece.class);

        assertThrows(IndexOutOfBoundsException.class, () -> {
            board.setPiece(piece, 8, 8);
        });
    }


    @Test
    public void removePiece_ValidPositionRow1Col1_Void() {
        int row = 1;
        int col = 1;

        Piece piece = Mockito.mock(Piece.class);
        board.setPiece(piece, row, col);
        
        board.removePiece(row, col);

        Piece result = board.getPieceAt(row, col);
        assertNull(result);
    }

    @Test
    public void removePiece_PositionExceedsBoardSizeRow10Col10_ThrowsIndexOutOfBoundsException() {
        assertThrows(IndexOutOfBoundsException.class, () -> {
            board.removePiece(10, 10);
        });
    }


    @Test
    public void isOccupied_ValidPositionRow0Col0_True() {
        Piece piece = Mockito.mock(Piece.class);
        board.setPiece(piece, 0, 0);

        boolean result = board.isOccupied(0, 0);

        assertTrue(result);
    }

    @Test
    public void isOccupied_ValidPositionRow0Col0_False() {
        Piece piece = Mockito.mock(Piece.class);
        board.setPiece(piece, 1, 1);

        boolean result = board.isOccupied(0, 0);

        assertFalse(result);
    }

    @Test
    public void isOccupied_PositionExceedsBoardSizeRow8Col8_ThrowsIndexOutOfBoundsException() {
        assertThrows(IndexOutOfBoundsException.class, () -> {
            board.isOccupied(8, 8);
        });
    }


    @Test
    public void isInTrap_ValidTrapPositionRow2Col2_True() {
        boolean result = board.isInTrap(2, 2);

        assertTrue(result);
    }

    @Test
    public void isInTrap_ValidNonTrapPositionRow4Col2_False() {
        boolean result = board.isInTrap(4, 2);

        assertFalse(result);
    }


    @Test
    public void isOneStep_ValidOneStepMoveRow0Col0ToRow0Col1_True() {
        boolean result = board.isOneStep(0, 0, 0, 1);
        assertTrue(result);
    }

    @Test
    public void isOneStep_ValidOneStepMoveRow0Col0ToRow1Col0_True() {
        boolean result = board.isOneStep(0, 0, 1, 0);
        assertTrue(result);
    }

    @Test
    public void isOneStep_InvalidTwoStepMoveRow0Col0ToRow0Col2_False() {
        boolean result = board.isOneStep(0, 0, 0, 2);
        assertFalse(result);
    }

    @Test
    public void isOneStep_InvalidTwoStepMoveRow0Col0ToRow2Col0_False() {
        boolean result = board.isOneStep(0, 0, 2, 0);
        assertFalse(result);
    }
    
    @Test
    public void isOneStep_InvalidDiagonalMoveRow0Col0ToRow1Col1_False() {
        boolean result = board.isOneStep(0, 0, 1, 1);
        assertFalse(result);
    }


    @Test
    public void isRabbitMoveValid_ValidMoveForGoldenRabbit_True() {
        Piece piece = Mockito.mock(Piece.class);
        Mockito.when(piece.getType()).thenReturn(PieceType.RABBIT);
        Mockito.when(piece.getColor()).thenReturn(PieceColor.GOLDEN);
        board.setPiece(piece, 0, 0);

        boolean result = board.isRabbitMoveValid(0, 0, 1, 0);

        assertTrue(result);
    }

    @Test
    public void isRabbitMoveValid_InvalidMoveForGoldenRabbit_False() {
        Piece piece = Mockito.mock(Piece.class);
        Mockito.when(piece.getType()).thenReturn(PieceType.RABBIT);
        Mockito.when(piece.getColor()).thenReturn(PieceColor.GOLDEN);
        board.setPiece(piece, 1, 0);

        boolean result = board.isRabbitMoveValid(1, 0, 0, 0);

        assertFalse(result);
    }

    @Test
    public void isRabbitMoveValid_ValidMoveForSilverRabbit_True() {
        Piece piece = Mockito.mock(Piece.class);
        Mockito.when(piece.getType()).thenReturn(PieceType.RABBIT);
        Mockito.when(piece.getColor()).thenReturn(PieceColor.SILVER);
        board.setPiece(piece, 7, 0);

        boolean result = board.isRabbitMoveValid(7, 0, 6, 0);

        assertTrue(result);
    }

    @Test
    public void isRabbitMoveValid_InvalidMoveForSilverRabbit_False() {
        Piece piece = Mockito.mock(Piece.class);
        Mockito.when(piece.getType()).thenReturn(PieceType.RABBIT);
        Mockito.when(piece.getColor()).thenReturn(PieceColor.SILVER);
        board.setPiece(piece, 6, 0);

        boolean result = board.isRabbitMoveValid(6, 0, 7, 0);

        assertFalse(result);
    }

    @Test
    public void isRabbitMoveValid_PositionExceedsBoardSizeRow8Col8_ThrowsIndexOutOfBoundsException() {
        assertThrows(IndexOutOfBoundsException.class, () -> {
            board.isRabbitMoveValid(8, 8, 9, 9);
        });
    }


    @Test
    public void getAdjacentPiecePositions_ValidPositionRow1Col1_ListOfSize4() {
        Piece piece = Mockito.mock(Piece.class);
        board.setPiece(piece, 0, 1);
        board.setPiece(piece, 1, 0);
        board.setPiece(piece, 2, 1);
        board.setPiece(piece, 1, 2);

        List<int[]> result = board.getAdjacentPiecePositions(1, 1);

        assertEquals(4, result.size());
    }

    @Test
    public void getAdjacentPiecePositions_ValidPositionRow0Col0_ListOfSize2() {
        Piece piece = Mockito.mock(Piece.class);
        board.setPiece(piece, 0, 1);
        board.setPiece(piece, 1, 0);

        List<int[]> result = board.getAdjacentPiecePositions(0, 0);

        assertEquals(2, result.size());
    }

    @Test
    public void getAdjacentPiecePositions_ValidPositionRow1Col1_NoAdjacentPieces() {
        Piece piece = Mockito.mock(Piece.class);
        board.setPiece(piece, 3, 3);
        board.setPiece(piece, 5, 4);
        List<int[]> result = board.getAdjacentPiecePositions(1, 1);

        assertEquals(0, result.size());
    }

    @Test
    public void getAdjacentPiecePositions_PositionExceedsBoardSizeRow8Col8_ThrowsIndexOutOfBoundsException() {
        assertThrows(IndexOutOfBoundsException.class, () -> {
            board.getAdjacentPiecePositions(8, 8);
        });
    }


    @Test
    public void hasAdjacentFriendlyPieces_ValidPositionRow1Col1_True() {
        Piece piece1 = Mockito.mock(Piece.class);
        Mockito.when(piece1.getColor()).thenReturn(PieceColor.GOLDEN);
        board.setPiece(piece1, 1, 1);

        Piece piece2 = Mockito.mock(Piece.class);
        Mockito.when(piece2.getColor()).thenReturn(PieceColor.GOLDEN);
        board.setPiece(piece2, 1, 2);

        boolean result = board.hasAdjacentFriendlyPieces(1, 1);

        assertTrue(result);
    }

    @Test
    public void hasAdjacentFriendlyPieces_ValidPositionRow1Col1_False() {
        Piece piece1 = Mockito.mock(Piece.class);
        Mockito.when(piece1.getColor()).thenReturn(PieceColor.GOLDEN);
        board.setPiece(piece1, 1, 1);

        Piece piece2 = Mockito.mock(Piece.class);
        Mockito.when(piece2.getColor()).thenReturn(PieceColor.SILVER);
        board.setPiece(piece2, 1, 2);

        boolean result = board.hasAdjacentFriendlyPieces(1, 1);

        assertFalse(result);
    }

    @Test
    public void hasAdjacentFriendlyPieces_ValidPositionRow1Col1_NoAdjacentPieces() {
        Piece piece1 = Mockito.mock(Piece.class);
        Mockito.when(piece1.getColor()).thenReturn(PieceColor.GOLDEN);
        board.setPiece(piece1, 1, 1);

        boolean result = board.hasAdjacentFriendlyPieces(1, 1);

        assertFalse(result);
    }

    @Test
    public void hasAdjacentFriendlyPieces_PositionExceedsBoardSizeRow8Col8_ThrowsIndexOutOfBoundsException() {
        assertThrows(IndexOutOfBoundsException.class, () -> {
            board.hasAdjacentFriendlyPieces(8, 8);
        });
    }


    @Test
    public void hasAdjacentEnemyPiecesWithHigherValue_ValidPositionRow1Col1_True() {
        Piece currentPiece = Mockito.mock(Piece.class);
        Mockito.when(currentPiece.getColor()).thenReturn(PieceColor.GOLDEN);
        Mockito.when(currentPiece.getPieceWeight()).thenReturn(1);
        board.setPiece(currentPiece, 1, 1);

        Piece enemyPiece = Mockito.mock(Piece.class);
        Mockito.when(enemyPiece.getColor()).thenReturn(PieceColor.SILVER);
        Mockito.when(enemyPiece.getPieceWeight()).thenReturn(2);
        board.setPiece(enemyPiece, 1, 2);

        boolean result = board.hasAdjacentEnemyPiecesWithHigherValue(1, 1);

        assertTrue(result);
    }

    @Test
    public void hasAdjacentEnemyPiecesWithHigherValue_ValidPositionRow1Col1_False() {
        Piece currentPiece = Mockito.mock(Piece.class);
        Mockito.when(currentPiece.getColor()).thenReturn(PieceColor.GOLDEN);
        Mockito.when(currentPiece.getPieceWeight()).thenReturn(2);
        board.setPiece(currentPiece, 1, 1);

        Piece enemyPiece = Mockito.mock(Piece.class);
        Mockito.when(enemyPiece.getColor()).thenReturn(PieceColor.SILVER);
        Mockito.when(enemyPiece.getPieceWeight()).thenReturn(1);
        board.setPiece(enemyPiece, 1, 2);

        boolean result = board.hasAdjacentEnemyPiecesWithHigherValue(1, 1);

        assertFalse(result);
    }

    @Test
    public void hasAdjacentEnemyPiecesWithHigherValue_ValidPositionRow1Col1_NoAdjacentPieces() {
        Piece currentPiece = Mockito.mock(Piece.class);
        Mockito.when(currentPiece.getColor()).thenReturn(PieceColor.GOLDEN);
        Mockito.when(currentPiece.getPieceWeight()).thenReturn(2);
        board.setPiece(currentPiece, 1, 1);

        boolean result = board.hasAdjacentEnemyPiecesWithHigherValue(1, 1);

        assertFalse(result);
    }

    @Test
    public void hasAdjacentEnemyPiecesWithHigherValue_PositionExceedsBoardSizeRow8Col8_ThrowsIndexOutOfBoundsException() {
        assertThrows(IndexOutOfBoundsException.class, () -> {
            board.hasAdjacentEnemyPiecesWithHigherValue(8, 8);
        });
    }


    @Test
    void isFrozen_ValidPositionRow1Col1_False() {
        Piece currentPiece = Mockito.mock(Piece.class);
        Mockito.when(currentPiece.getColor()).thenReturn(PieceColor.GOLDEN);
        Mockito.when(currentPiece.getPieceWeight()).thenReturn(1);
        board.setPiece(currentPiece, 1, 1);

        // Assuming that the piece at (1,1) is surrounded by enemy pieces with higher weight and no friendly pieces adjacent
        assertFalse(board.isFrozen(1, 1));
    }

    @Test
    void isFrozen_ValidPositionRow2Col2WithStrongerEnemyPieceRow2Col1_True() {
        Piece currentPiece = Mockito.mock(Piece.class);
        Mockito.when(currentPiece.getColor()).thenReturn(PieceColor.GOLDEN);
        Mockito.when(currentPiece.getPieceWeight()).thenReturn(1);
        board.setPiece(currentPiece, 2, 2);

        Piece enemyPiece = Mockito.mock(Piece.class);
        Mockito.when(enemyPiece.getColor()).thenReturn(PieceColor.SILVER);
        Mockito.when(enemyPiece.getPieceWeight()).thenReturn(2);
        board.setPiece(enemyPiece, 2, 1);

        // Assuming that the piece at (2,2) is not surrounded by enemy pieces with higher weight or has friendly pieces adjacent
        assertTrue(board.isFrozen(2, 2));
    }

    @Test
    void isFrozen_ValidPositionRow2Col2WithEqualEnemyPieceRow2Col1_False() {
        Piece currentPiece = Mockito.mock(Piece.class);
        Mockito.when(currentPiece.getColor()).thenReturn(PieceColor.GOLDEN);
        Mockito.when(currentPiece.getPieceWeight()).thenReturn(1);
        board.setPiece(currentPiece, 2, 2);

        Piece enemyPiece = Mockito.mock(Piece.class);
        Mockito.when(enemyPiece.getColor()).thenReturn(PieceColor.SILVER);
        Mockito.when(enemyPiece.getPieceWeight()).thenReturn(1);
        board.setPiece(enemyPiece, 2, 1);

        // Assuming that the piece at (2,2) is not surrounded by enemy pieces with higher weight or has friendly pieces adjacent
        assertFalse(board.isFrozen(2, 2));
    }

    @Test
    void isFrozen_ValidPositionRow2Col2WithFriendlyPieceRow2Col1AndEnemyStrongerPieceRow1Col2_False() {
        Piece currentPiece = Mockito.mock(Piece.class);
        Mockito.when(currentPiece.getColor()).thenReturn(PieceColor.GOLDEN);
        Mockito.when(currentPiece.getPieceWeight()).thenReturn(1);
        board.setPiece(currentPiece, 2, 2);

        Piece friendlyPiece = Mockito.mock(Piece.class);
        Mockito.when(friendlyPiece.getColor()).thenReturn(PieceColor.GOLDEN);
        Mockito.when(friendlyPiece.getPieceWeight()).thenReturn(1);
        board.setPiece(friendlyPiece, 2, 1);


        Piece enemyPiece = Mockito.mock(Piece.class);
        Mockito.when(enemyPiece.getColor()).thenReturn(PieceColor.SILVER);
        Mockito.when(enemyPiece.getPieceWeight()).thenReturn(2);
        board.setPiece(enemyPiece, 1, 2);


        // Assuming that the piece at (2,2) is not surrounded by enemy pieces with higher weight or has friendly pieces adjacent
        assertFalse(board.isFrozen(2, 2));
    }

    @Test
    void isFrozen_OutOfBounds_ThrowsIndexOutOfBoundsException() {
        assertThrows(IndexOutOfBoundsException.class, () -> board.isFrozen(10, 10));
    }

 
    @Test
    void movePiece_ValidMoveFromRow2Col2ToRow3Col2_Void() {
        Piece currentPiece = Mockito.mock(Piece.class);
        Mockito.when(currentPiece.getColor()).thenReturn(PieceColor.GOLDEN);
        Mockito.when(currentPiece.getType()).thenReturn(PieceType.RABBIT);
        Mockito.when(currentPiece.getPieceWeight()).thenReturn(1);
        
        board.setPiece(currentPiece, 2, 2);
        assertDoesNotThrow(() -> board.movePiece(2, 2, 3, 2, false));
    }

    @Test
    void movePiece_InvalidMoveFromRow2Col2ToRow3Col3_IllegalArgumentException() {
        Piece currentPiece = Mockito.mock(Piece.class);
        Mockito.when(currentPiece.getColor()).thenReturn(PieceColor.GOLDEN);
        Mockito.when(currentPiece.getType()).thenReturn(PieceType.RABBIT);
        Mockito.when(currentPiece.getPieceWeight()).thenReturn(1);

        board.setPiece(currentPiece, 2, 2);
        assertThrows(IllegalArgumentException.class, () -> board.movePiece(2, 2, 3, 3, false));
    }

    @Test
    void movePiece_ValidMovePieceNewPositionOccupiedFromRow2Col2ToRow3Col2FrozenPiece_IllegalArgumentException() {
        Piece currentPiece = Mockito.mock(Piece.class);
        Mockito.when(currentPiece.getColor()).thenReturn(PieceColor.GOLDEN);
        Mockito.when(currentPiece.getPieceWeight()).thenReturn(1);
        board.setPiece(currentPiece, 2, 2);

        Piece enemyPiece = Mockito.mock(Piece.class);
        Mockito.when(enemyPiece.getColor()).thenReturn(PieceColor.SILVER);
        Mockito.when(enemyPiece.getPieceWeight()).thenReturn(2);
        board.setPiece(enemyPiece, 1, 2);
        

        board.setPiece(currentPiece, 2, 2);
        board.setPiece(enemyPiece, 1, 2);

        assertThrows(IllegalArgumentException.class, () -> board.movePiece(2, 2, 3, 2, false));
    }

    @Test
    void movePiece_NoPieceAtLocationRow2Col2_IllegalArgumentException() {
        assertThrows(IllegalArgumentException.class, () -> board.movePiece(2, 2, 3, 2, false));
    }

    @Test
    void movePiece_ValidMovePieceNewPositionOccupiedFromRow2Col2ToRow3Col2_IllegalArgumentException() {
        assertThrows(IllegalArgumentException.class, () -> board.movePiece(2, 2, 3, 2, false));
    }
}